package cz.unit4.service;


import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import cz.unit4.coda.CodaAccount;
import cz.unit4.coda.CodaCompany;
import cz.unit4.coda.CodaDocument;
import cz.unit4.coda.ListOfCodaDocuments;
import cz.unit4.codaws.CodaAccountWS;
import cz.unit4.codaws.CodaCompanyWS;
import cz.unit4.codaws.CodaDocumentWS;

@Path("/service")
@Produces({"application/json"})
public class SimpleRestService {
	@Context ServletContext ctx;
	static StringBuffer accountServiceURL= new StringBuffer();
	static StringBuffer accountServiceURI= new StringBuffer();
	static StringBuffer inputServiceURL= new StringBuffer();
	static StringBuffer inputServiceURI= new StringBuffer();
	
	static StringBuffer companyServiceURL= new StringBuffer();
	static StringBuffer companyServiceURI= new StringBuffer();
	
	
	static boolean recordInputJSON=false;
	static int proxiesTimeout = 0;	//will mean infinitive
	static int proxiesPoolSize=10000; //if not set it will be 10000. and if 0 it will be anyway 10 000

	//private static final Logger logger = Logger.getLogger(SimpleRestService.class);
	
    static final Logger logger = LogManager.getLogger(SimpleRestService.class.getName());

	@GET
	@Path("/getSomething")
    @Produces(MediaType.TEXT_PLAIN)
	public String getSomething(@QueryParam("request") String request ,
			 @DefaultValue("1") @QueryParam("version") int version) {

		if (logger.isDebugEnabled()) {
			logger.debug("Start getSomething");
			logger.debug("data: '" + request + "'");
			logger.debug("version: '" + version + "'");
		}

		String response = null;

        try{			
            switch(version){
	            case 1:
	                if(logger.isDebugEnabled()) logger.debug("in version 1");

	                response = "Response from Jersey Restful Webservice : " + request;
                    break;
                default: throw new Exception("Unsupported version: " + version);
            }
        }
        catch(Exception e){
        	response = e.getMessage().toString();
        }
        
        if(logger.isDebugEnabled()){
            logger.debug("result: '"+response+"'");
            logger.debug("End getSomething");
        }
        return response;	
	}

	@POST
	@Path("/postSomething")
    @Produces(MediaType.TEXT_PLAIN)
	public String postSomething(@FormParam("request") String request ,  @DefaultValue("1") @FormParam("version") int version) {

		if (logger.isDebugEnabled()) {
			logger.debug("Start postSomething");
			logger.debug("data: '" + request + "'");
			logger.debug("version: '" + version + "'");
		}

		String response = null;

        try{			
            switch(version){
	            case 1:
	                if(logger.isDebugEnabled()) logger.debug("in version 1");

	                response = "Response from Jersey Restful Webservice : " + request;
                    break;
                default: throw new Exception("Unsupported version: " + version);
            }
        }
        catch(Exception e){
        	response = e.getMessage().toString();
        }
        
        if(logger.isDebugEnabled()){
            logger.debug("result: '"+response+"'");
            logger.debug("End postSomething");
        }
        return response;	
	}

	@PUT
	@Path("/putSomething")
    @Produces(MediaType.TEXT_PLAIN)
	public String putSomething(@FormParam("request") String request ,  @DefaultValue("1") @FormParam("version") int version) {
		if (logger.isDebugEnabled()) {
			logger.debug("Start putSomething");
			logger.debug("data: '" + request + "'");
			logger.debug("version: '" + version + "'");
		}

		String response = null;

        try{			
            switch(version){
	            case 1:
	                if(logger.isDebugEnabled()) logger.debug("in version 1");

	                response = "Response from Jersey Restful Webservice : " + request;
                    break;
                default: throw new Exception("Unsupported version: " + version);
            }
        }
        catch(Exception e){
        	response = e.getMessage().toString();
        }
        
        if(logger.isDebugEnabled()){
            logger.debug("result: '"+response+"'");
            logger.debug("End putSomething");
        }
        return response;	
	}

	@DELETE
	@Path("/deleteSomething")
	public void deleteSomething(@FormParam("request") String request ,  @DefaultValue("1") @FormParam("version") int version) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Start deleteSomething");
			logger.debug("data: '" + request + "'");
			logger.debug("version: '" + version + "'");
		}


        try{			
            switch(version){
	            case 1:
	                if(logger.isDebugEnabled()) logger.debug("in version 1");

                    break;
                default: throw new Exception("Unsupported version: " + version);
            }
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        
        if(logger.isDebugEnabled()){
            logger.debug("End deleteSomething");
        }
	}
	
	
	//Post Text - try to post text 
	@POST
	@Path("/postText")
    @Produces(MediaType.TEXT_PLAIN)
	public String postCODA(@FormParam("request") String request ,  @DefaultValue("1") @FormParam("version") int version) {

		if (logger.isDebugEnabled()) {
			logger.debug("Start posttext");
			logger.debug("data: '" + request + "'");
			logger.debug("version: '" + version + "'");
		}

		String response = null;

        try{			
            switch(version){
	            case 1:
	                if(logger.isDebugEnabled()) logger.debug("in version 1");

	                response = "Response from Jersey Restful Webservice : " + request;
                    break;
                default: throw new Exception("Unsupported version: " + version);
            }
        }
        catch(Exception e){
        	response = e.getMessage().toString();
        }
        
        if(logger.isDebugEnabled()){
            logger.debug("result: '"+response+"'");
            logger.debug("End postSomething");
        }
        return response;	
	}
	
	
	/**
	 * This method is my try to use GET - so far it is just example
	 * @return
	 */
	@GET
	@Path("/getJsonToCoda")
	@Produces(MediaType.APPLICATION_JSON)
	public CodaDocument JsonToCoda() {

		CodaDocument document = new CodaDocument();
		document.setCmpCode("Enter Sandman");
		document.setDocCode("Metallica");

		return document;

	}
	
	//Above this was just examples, now real job...

	/**
	 * Posts one ore more documents to CODA
	 * 
	 * @param document (ListOfCodaDocuments)
	 * @return
	 */
	@POST
	@Path("/postDocumentToCoda")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postDocumentToCoda(@Context HttpHeaders headers, ListOfCodaDocuments documents) {
		//int succProcessedDocs=0;
		
		try{
			//Reading web.xml for configuration parameters if they are not already there in static variables
			if (inputServiceURL==null || inputServiceURL.toString().isEmpty()){
				if (ctx.getInitParameter("InputServiceURL")!=null){
					inputServiceURL.append(ctx.getInitParameter("InputServiceURL"));
					logger.debug("Input service URL:" + inputServiceURL );
				} else {
					logger.error("Error in postDocumentToCoda (Can't find Input service URL)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Input service URL","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
				if (ctx.getInitParameter("InputServiceURI")!=null){
					inputServiceURI.append(ctx.getInitParameter("InputServiceURI"));
				} else {
					logger.error("Error in postDocumentToCoda (Can't find Input service URI)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Input service URI","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
			
				//RecordInputJSON
				if (ctx.getInitParameter("RecordInputJSON")!=null){
					if (ctx.getInitParameter("RecordInputJSON").equalsIgnoreCase("true")){
						recordInputJSON=true;
					}
				}
			
				//proxiesTimeout
				if (ctx.getInitParameter("ProxiesTimeout")!=null){
					try {
						proxiesTimeout = Integer.parseInt(ctx.getInitParameter("ProxiesTimeout"));
					} catch (Exception e){}
				} 
				
				//proxiesPoolSize
				if (ctx.getInitParameter("ProxiesPoolSize")!=null){
					try {
						proxiesPoolSize = Integer.parseInt(ctx.getInitParameter("ProxiesPoolSize"));
					} catch (Exception e){}
				} 
				
			} //end of if read init parameters
			
			//Reading header for authentication
			List<String> authHeaders = headers.getRequestHeader(HttpHeaders.AUTHORIZATION);
			if (authHeaders!=null){
				String authorization = authHeaders.get(0);
				String base64Credentials = authorization.substring("Basic".length()).trim();
				byte[] decoded = DatatypeConverter.parseBase64Binary(base64Credentials);
				String credentials = new String(decoded, "UTF-8");
		        final String[] values = credentials.split(":",2);
		        AuthenticateInformation ainfo= new AuthenticateInformation(values[0],values[1]);
		        
		        //Create the CODADocumentWS object
				CodaDocumentWS codaDocumentWS = new CodaDocumentWS(inputServiceURL.toString(),inputServiceURI.toString(),ainfo.getUserCODA(),ainfo.getPasswordCODA(),documents.getDocuments().get(0).getCmpCode(), documents.getToken());
				String lastToken=codaDocumentWS.getLatsToken();
				
				//post document		
				JSONObject response =codaDocumentWS.PostJSONtoCODA(documents);
				
				
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,response,lastToken);
				String strJWsResp = jsWsResp.toString();
				//Now when it is String it contains escape characters and additional " they must be removed so that it is correct JSON String
				strJWsResp= strJWsResp.replace("\"{", "{"); //means "{ by {
				strJWsResp= strJWsResp.replace("}\"", "}"); //means "}" by }
				strJWsResp= strJWsResp.replace("\\\"", "\"");
				
				if (recordInputJSON){
					//Example when the input document is returned
					//JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,documents);
					logger.info(documents.toString());
					
				}

				return Response.status(201).entity(strJWsResp).build();
				
			} else {
				//Here we will need to create the response saying that no authentication has been provided
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-1,"NO AUTHENTICATION PROVIDED","");
				return Response.status(201).entity(jsWsResp.toString()).build();
			}//if there is no authentication provided in headers
		} catch (Exception e){
			try{
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR " + e.getMessage(),"");
				logger.error("TECHNICAL ERROR in postDocumentToCoda " + e.getMessage());
				return Response.status(201).entity(jsWsResp.toString()).build();
			} catch (Exception ex){}
			return Response.status(201).entity("TECHNICAL ERROR " + e.getMessage()).build();
		} 
	}//End of postDocumentToCoda
	
	
	/**
	 * Validates account in CODA
	 * @param headers
	 * @param account (CodaAccount account)
	 * @return
	 */
	
	@POST
	@Path("/validateCodaAccount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response validateCodaAccount(@Context HttpHeaders headers, CodaAccount account) {
		//StringBuffer accountServiceURL= new StringBuffer();
		//StringBuffer accountServiceURI= new StringBuffer();
		try{
			
			//Reading web.xml for configuration parameters
			
			if (accountServiceURL==null || accountServiceURL.toString().isEmpty()){
				if (ctx.getInitParameter("InputServiceURL")!=null){
					accountServiceURL.append(ctx.getInitParameter("AccountServiceURL"));
					logger.debug("Account service URL:" + accountServiceURL );
				} else {
					logger.error("Error in validateCodaAccount (Can't find Account service URL)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Account service URL","");
					return Response.status(201).entity(jsWsResp.toString()).build();
					
				}
				
				if (ctx.getInitParameter("AccountServiceURI")!=null){
					accountServiceURI.append(ctx.getInitParameter("AccountServiceURI"));
				} else {
					logger.error("Error in validateCodaAccount (Can't find Account service URI)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Account service URI","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
				//RecordInputJSON
				//boolean recordInputJSON=false;
				if (ctx.getInitParameter("RecordInputJSON")!=null){
					if (ctx.getInitParameter("RecordInputJSON").equalsIgnoreCase("true")){
						recordInputJSON=true;
					}
				}
				
				if (ctx.getInitParameter("ProxiesTimeout")!=null){
					try {
						proxiesTimeout = Integer.parseInt(ctx.getInitParameter("ProxiesTimeout"));
					} catch (Exception e){}
				} 
				
				//proxiesPoolSize
				if (ctx.getInitParameter("ProxiesPoolSize")!=null){
					try {
						proxiesPoolSize = Integer.parseInt(ctx.getInitParameter("ProxiesPoolSize"));
					} catch (Exception e){}
				} 
				
				
			}//end  reading config parameters
			
			List<String> authHeaders = headers.getRequestHeader(HttpHeaders.AUTHORIZATION);
			if (authHeaders!=null){
				String authorization = authHeaders.get(0);
				String base64Credentials = authorization.substring("Basic".length()).trim();
				byte[] decoded = DatatypeConverter.parseBase64Binary(base64Credentials);
				String credentials = new String(decoded, "UTF-8");
		        final String[] values = credentials.split(":",2);
		        AuthenticateInformation ainfo= new AuthenticateInformation(values[0],values[1]);
		        
		        //Create the CodaAccountWS object
				CodaAccountWS codaAccountWS = new CodaAccountWS(accountServiceURL.toString(),accountServiceURI.toString(),ainfo.getUserCODA(),ainfo.getPasswordCODA(),account.getCmpCode(),account.getToken());
				
				//post document		
				JSONObject response =codaAccountWS.ValidateAccountInCODA(account);
				String lastToken=codaAccountWS.getLatsToken();
				
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,response,lastToken);
				String strJWsResp = jsWsResp.toString();
				//Now when it is String it contains escape characters and additional " they must be removed so that it is correct JSON String
				strJWsResp= strJWsResp.replace("\"{", "{"); //means "{ by {
				strJWsResp= strJWsResp.replace("}\"", "}"); //means "}" by }
				strJWsResp= strJWsResp.replace("\\\"", "\"");
				
				if (recordInputJSON){
					//Example when the input document is returned
					//JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,documents);
					logger.info(account.toString());
				}

				return Response.status(201).entity(strJWsResp).build();
		        
			} else {
				//Here we will need to create the response saying that no authentication has been provided
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-1,"NO AUTHENTICATION PROVIDED","");
				return Response.status(201).entity(jsWsResp.toString()).build();
			}//if there is no authentication provided in headers
		} catch (Exception e){
			try{
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR " + e.getMessage(),"");
				logger.error("TECHNICAL ERROR in validateCodaAccount " + e.getMessage());
				return Response.status(201).entity(jsWsResp.toString()).build();
			} catch (Exception ex){}
			return Response.status(201).entity("TECHNICAL ERROR " + e.getMessage()).build();
		} 
		
	} //End of validateCodaAccount
	
	/**
	 * Updates existing document in Intray
	 * @param headers
	 * @param documents
	 * @return
	 */
	
	@POST
	@Path("/updateDocumentInCoda")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateDocumentInCoda(@Context HttpHeaders headers, ListOfCodaDocuments documents) {
		try{
			//Reading web.xml for configuration parameters if they are not already there in static variables
			if (inputServiceURL==null || inputServiceURL.toString().isEmpty()){
				if (ctx.getInitParameter("InputServiceURL")!=null){
					inputServiceURL.append(ctx.getInitParameter("InputServiceURL"));
					logger.debug("Input service URL:" + inputServiceURL );
				} else {
					logger.error("Error in updateDocumentInCoda (Can't find Input service URL)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Input service URL","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
				if (ctx.getInitParameter("InputServiceURI")!=null){
					inputServiceURI.append(ctx.getInitParameter("InputServiceURI"));
				} else {
					logger.error("Error in updateDocumentInCoda (Can't find Input service URI)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Input service URI","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
			
				//RecordInputJSON
				if (ctx.getInitParameter("RecordInputJSON")!=null){
					if (ctx.getInitParameter("RecordInputJSON").equalsIgnoreCase("true")){
						recordInputJSON=true;
					}
				}
			
				//proxiesTimeout
				if (ctx.getInitParameter("ProxiesTimeout")!=null){
					try {
						proxiesTimeout = Integer.parseInt(ctx.getInitParameter("ProxiesTimeout"));
					} catch (Exception e){}
				} 
				
				//proxiesPoolSize
				if (ctx.getInitParameter("ProxiesPoolSize")!=null){
					try {
						proxiesPoolSize = Integer.parseInt(ctx.getInitParameter("ProxiesPoolSize"));
					} catch (Exception e){}
				} 
				
			} //end of if read init parameters
			
			//Reading header for authentication
			List<String> authHeaders = headers.getRequestHeader(HttpHeaders.AUTHORIZATION);
			if (authHeaders!=null){
				String authorization = authHeaders.get(0);
				String base64Credentials = authorization.substring("Basic".length()).trim();
				byte[] decoded = DatatypeConverter.parseBase64Binary(base64Credentials);
				String credentials = new String(decoded, "UTF-8");
		        final String[] values = credentials.split(":",2);
		        AuthenticateInformation ainfo= new AuthenticateInformation(values[0],values[1]);
		        

		        //Create the CODADocumentWS object, it is the same object as is for Input
				CodaDocumentWS codaDocumentWS = new CodaDocumentWS(inputServiceURL.toString(),inputServiceURI.toString(),ainfo.getUserCODA(),ainfo.getPasswordCODA(),documents.getDocuments().get(0).getCmpCode(), documents.getToken());
				String lastToken=codaDocumentWS.getLatsToken();
				
				//Update of the document
				JSONObject response =codaDocumentWS.UpdateJSONtoCODA(documents);
				
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,response,lastToken);
				String strJWsResp = jsWsResp.toString();
				//Now when it is String it contains escape characters and additional " they must be removed so that it is correct JSON String
				strJWsResp= strJWsResp.replace("\"{", "{"); //means "{ by {
				strJWsResp= strJWsResp.replace("}\"", "}"); //means "}" by }
				strJWsResp= strJWsResp.replace("\\\"", "\"");
				
				if (recordInputJSON){
					//Example when the input document is returned
					//JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,documents);
					logger.info(documents.toString());
				}

				return Response.status(201).entity(strJWsResp).build();
				
			} else {
				//Here we will need to create the response saying that no authentication has been provided
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-1,"NO AUTHENTICATION PROVIDED","");
				return Response.status(201).entity(jsWsResp.toString()).build();
			}//if there is no authentication provided in headers
		} catch (Exception e){
			try{
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2, e.getMessage(),"");
				logger.error("TECHNICAL ERROR in updateDocumentInCoda " + e.getMessage());
				return Response.status(201).entity(jsWsResp.toString()).build();
			} catch (Exception ex){}
			return Response.status(201).entity("TECHNICAL ERROR " + e.getMessage()).build();
		} 
		
	} //End of updateDocumentInCoda
	
	
	@POST
	@Path("/checkAccessToCoda")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkAccessToCoda(@Context HttpHeaders headers, CodaCompany company) {
		try{
			
			//Reading web.xml for configuration parameters if they are not already there in static variables
			if (companyServiceURL==null || companyServiceURL.toString().isEmpty()){
				if (ctx.getInitParameter("CompanyServiceURL")!=null){
					companyServiceURL.append(ctx.getInitParameter("CompanyServiceURL"));
					logger.debug("Company service URL:" + companyServiceURL );
				} else {
					logger.error("Error in checkAccessToCoda (Can't find Company service URL)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Company service URL","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
				if (ctx.getInitParameter("CompanyServiceURI")!=null){
					companyServiceURI.append(ctx.getInitParameter("CompanyServiceURI"));
				} else {
					logger.error("Error in checkAccessToCoda (Can't find Company service URI)");
					JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2,"TECHNICAL ERROR - Can't find Company service URI","");
					return Response.status(201).entity(jsWsResp.toString()).build();
				}
			} //End of reading web.xml
			//Reading header for authentication
			List<String> authHeaders = headers.getRequestHeader(HttpHeaders.AUTHORIZATION);
			if (authHeaders!=null){
				String authorization = authHeaders.get(0);
				String base64Credentials = authorization.substring("Basic".length()).trim();
				byte[] decoded = DatatypeConverter.parseBase64Binary(base64Credentials);
				String credentials = new String(decoded, "UTF-8");
		        final String[] values = credentials.split(":",2);
		        AuthenticateInformation ainfo= new AuthenticateInformation(values[0],values[1]);

				CodaCompanyWS codaCopanyWS = new CodaCompanyWS(companyServiceURL.toString(),companyServiceURI.toString(),ainfo.getUserCODA(),ainfo.getPasswordCODA(),company.getCmpCode());
				
				//List of the companies
				JSONObject response =codaCopanyWS.getListOfCompanies();
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)0,response,null);
				String strJWsResp = jsWsResp.toString();
				//Now when it is String it contains escape characters and additional " they must be removed so that it is correct JSON String
				strJWsResp= strJWsResp.replace("\"{", "{"); //means "{ by {
				strJWsResp= strJWsResp.replace("}\"", "}"); //means "}" by }
				strJWsResp= strJWsResp.replace("\\\"", "\"");
				
				if (recordInputJSON){
					logger.info(company.toString());
				}

				return Response.status(201).entity(strJWsResp).build();
			} else {
				//Here we will need to create the response saying that no authentication has been provided
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-1,"NO AUTHENTICATION PROVIDED","");
				return Response.status(201).entity(jsWsResp.toString()).build();
			}//if there is no authentication provided in headers
			
		} catch (Exception e){
			try{
				JsCodaWsResponse jsWsResp = new JsCodaWsResponse((short)-2, e.getMessage(),"");
				logger.error("TECHNICAL ERROR in checkAccessToCoda: " + e.getMessage());
				return Response.status(201).entity(jsWsResp.toString()).build();
			} catch (Exception ex){}
			return Response.status(201).entity("TECHNICAL ERROR in checkAccessToCoda: " + e.getMessage()).build();
		} 

	} //checkAccessToCoda
	
	
	
}
