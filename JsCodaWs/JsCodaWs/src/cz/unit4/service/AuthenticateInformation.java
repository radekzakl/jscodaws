package cz.unit4.service;

/**
 * Holds all the user/password for CODA 
 * @author RZakl
 *
 */
public class AuthenticateInformation {
	private static String userCODA=null;
	private static String passwordCODA=null;
	
	public String getUserCODA(){
		return AuthenticateInformation.userCODA;
	}
	public void setUserCODA(String userCODA){
		AuthenticateInformation.userCODA=userCODA;
	}

	public String getPasswordCODA(){
		return AuthenticateInformation.passwordCODA;
	}
	public void setPasswordCODA(String passwordCODA){
		AuthenticateInformation.passwordCODA=passwordCODA;
	}

	public AuthenticateInformation(String userCODA, String passwordCODA){
		AuthenticateInformation.userCODA=userCODA;
		AuthenticateInformation.passwordCODA=passwordCODA;
	}
	
}

