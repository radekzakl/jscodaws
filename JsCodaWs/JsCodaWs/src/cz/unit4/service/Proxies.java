package cz.unit4.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Proxies {
		LinkedHashMap<String,OneProxy> availableProxies= new LinkedHashMap<String,OneProxy>(){
	        /**
			 * 
			 */
			private static final long serialVersionUID = -1620336678818731347L;

			@Override
	        protected boolean removeEldestEntry(final Map.Entry eldest) {
	            return size() > SimpleRestService.proxiesPoolSize;
	        }
	    };
				
				;
		
		public void setAvailableProxies(LinkedHashMap<String,OneProxy> availableProxies){
			this.availableProxies = availableProxies;
		}

		public HashMap<String,OneProxy> getAvailableProxies(){
				return this.availableProxies;
		}
		
		public static String tokenGenerator(int count){
			final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			StringBuilder builder = new StringBuilder();

			while (count-- != 0) {
				int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
				builder.append(ALPHA_NUMERIC_STRING.charAt(character));
			}
			return builder.toString();
		}
		
		/**
		 * Clears all proxies which are here for long time. Whats long will be done by parameter
		 */
		public void ClearOlderProxies(){
			if (SimpleRestService.proxiesTimeout !=0){
				if (!this.availableProxies.entrySet().isEmpty()){
		
					Iterator<Map.Entry<String,OneProxy>> iter = this.availableProxies.entrySet().iterator();
					while (iter.hasNext()) {
					    Map.Entry<String,OneProxy> entry = iter.next();
					    
					    //String hash = entry.getKey();
					    OneProxy proxy = entry.getValue();
					    Date nowIs = new Date();
					    if ((nowIs.getTime() - proxy.getTimeCreated().getTime())>SimpleRestService.proxiesTimeout*1000){
					    	//deleting them
					    	iter.remove();
					    }
					}
				
				}
			}//if timeout is not 0 - means unlimited
		} //End of clearing Old proxies
		
}
