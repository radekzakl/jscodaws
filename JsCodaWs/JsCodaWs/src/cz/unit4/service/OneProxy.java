package cz.unit4.service;

import java.util.Date;

import com.coda.efinance.schemas.inputext.input_13_0.webservice.InputServicePortTypes;
import com.coda.efinance.schemas.transaction.account_12_0.webservice.AccountServicePortTypes;

public class OneProxy {
	private String token=null;
	private InputServicePortTypes proxyInput = null;
	private AccountServicePortTypes proxyAccount = null;
	private Date timeCreated = new Date();
	
	public void setToken(String token) {this.token=token;}
	public String getToken(){return this.token;}
	
	public void setProxyInput(InputServicePortTypes proxyInput){this.proxyInput=proxyInput; }
	public InputServicePortTypes getProxyInput(){ return this.proxyInput; }

	public void setProxyAccount(AccountServicePortTypes proxyAccount){this.proxyAccount=proxyAccount; }
	public AccountServicePortTypes getProxyAccount(){ return this.proxyAccount; }
	
	public void setTimeCreated(Date timeCreated){
		this.timeCreated = timeCreated;
	}
	public Date getTimeCreated(){return this.timeCreated;}
	
	
	public OneProxy(String token, InputServicePortTypes proxyInput,AccountServicePortTypes proxyAccount){
		this.token=token;
		this.proxyInput = proxyInput;
		this.proxyAccount=proxyAccount;
		this.timeCreated=new Date();		//id created we create the new date as now.
	}
	
}
