package cz.unit4.service;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Encapsulates the response of the call to the CODA Web Service
 *  - responseStat tells the user if call was technically successfull
 *  - wsResponse contains the result of the call returned from CODA Ws
 * @author RZakl
 * @date 14.9.2017
 *
 */
public class JsCodaWsResponse {
	private short responseStat=0;	//0 - ok, < 0 various error statuses  
	private Object wsResponse=null; //response of Coda Ws converted to JSON
	private String token=null;		//if call is successful it returns my token which I use to retrieve right existing proxy
	
	public JsCodaWsResponse(short responseStat,Object wsResponse, String token){
		this.responseStat=responseStat;
		this.token=token;
		
		if (wsResponse.getClass().getName().equalsIgnoreCase("org.json.JSONObject")){
			//can't serialize JSON for some reason later so I do it as String here
			this.wsResponse=wsResponse.toString();
		} else {
			this.wsResponse=wsResponse;
		}
		
		//this.wsResponse=wsResponse;
	}

	public Object getWsResponse() {return wsResponse;}
	public void setWsResponse (Object wsResponse){
		if (wsResponse.getClass().getName().equalsIgnoreCase("org.json.JSONObject")){
			//can't serialize JSON for some reason later so I do it as String here
			this.wsResponse=wsResponse.toString();
		} else {
			this.wsResponse=wsResponse;
		}
		
		
	}
	
	public short getResponseStat(){return this.responseStat;}
	public void setResponseStat(short responseStat){
		this.responseStat=responseStat;
	}
	
	public String getToken(){return this.token;}
	public void setToken(String token){this.token =token;}
	

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		String thisAsString = null;
		try {
			thisAsString = mapper.writeValueAsString(this);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return thisAsString;
	} //End of toString
	
}
