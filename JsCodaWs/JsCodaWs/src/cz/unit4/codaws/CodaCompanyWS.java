package cz.unit4.codaws;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Iterator;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.coda.efinance.schemas.common.GlobalKey;
import com.coda.efinance.schemas.common.KeyData;
import com.coda.efinance.schemas.common.KeyDataElement;
import com.coda.efinance.schemas.company.ReqKeys;
import com.coda.efinance.schemas.company.company_13_0.webservice.CompanyService;
import com.coda.efinance.schemas.company.company_13_0.webservice.CompanyServicePortTypes;
import com.coda.efinance.schemas.company.company_13_0.webservice.ListRequest;
import com.coda.efinance.schemas.company.company_13_0.webservice.ListResponse;

import cz.unit4.coda.CodaCompany;
import cz.unit4.service.Proxies;

public class CodaCompanyWS {
	
	private CompanyServicePortTypes proxy;
	public static Proxies proxies = new Proxies();
	//private String lastToken=null;
	

	static final Logger logger = LogManager.getLogger(CodaAccountWS.class.getName());
	
	public CodaCompanyWS (String companyServiceURL,String companyServiceURI, String userCODA, String passwordCODA, String cmpcode) throws MalformedURLException{
		String strWSDL  = companyServiceURL;
		URL url = new URL(strWSDL);
		QName qname = new QName(companyServiceURI,"CompanyService","");
		CompanyService service = new CompanyService(url,qname);
		
		proxy= service.getCompanyServicePort();
		
		BindingProvider prov = (BindingProvider)proxy;
		StringBuffer strUserCmp = new StringBuffer(userCODA);
		strUserCmp.append("\\");
		strUserCmp.append(cmpcode);
		prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, strUserCmp.toString());
		prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, passwordCODA);
	} //end of constructor
	
	
	/**
	 * Return the list of available companies
	 * @return
	 * @throws JSONException
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	public JSONObject getListOfCompanies() throws JSONException, ParseException, DatatypeConfigurationException {
		StringBuffer strResponses = new StringBuffer();
		
		try {
			ListRequest lst= new ListRequest();
			ReqKeys keys= new ReqKeys();
			GlobalKey gk = new GlobalKey();
			gk.setCode("*");
			keys.setWildKey(gk);
			lst.setFilter(keys);
			ListResponse resp = proxy.list(lst);
			KeyData keyData = resp.getKeys();
			Iterator<KeyDataElement> it = keyData.getKeies().iterator();
			strResponses.append("<getListOfCompanies>");
			
			while (it.hasNext()){
				KeyDataElement element = it.next();
				strResponses.append("<company>");
				strResponses.append(element.getCode());
				strResponses.append("</company>");
			}
			strResponses.append("</getListOfCompanies>");
			
		} catch (Exception e){
			strResponses.append("<getListOfCompaniesError>");
			strResponses.append(e.getMessage());
			strResponses.append("</getListOfCompaniesError>");
		}

         JSONObject xmlJSONObj = XML.toJSONObject(strResponses.toString());
         return xmlJSONObj;
	} //End of
	

}
