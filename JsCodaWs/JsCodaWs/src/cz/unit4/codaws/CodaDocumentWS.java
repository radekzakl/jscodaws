package cz.unit4.codaws;


import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.coda.efinance.schemas.common.TypeCtDocDest;
import com.coda.efinance.schemas.common.TypeCtDocLineDrCrB;
import com.coda.efinance.schemas.common.TypeCtDocLineTypeB;
import com.coda.efinance.schemas.inputext.DocumentWideData;
import com.coda.efinance.schemas.inputext.InputPostDocument;
//import com.coda.efinance.schemas.input.input_13_0.webservice.InputService;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.InputService;
//import com.coda.efinance.schemas.input.input_13_0.webservice.InputServicePortTypes;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.InputServicePortTypes;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.IntrayLoadRequest;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.IntrayLoadResponse;
//import com.coda.efinance.schemas.input.input_13_0.webservice.PostRequest;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.PostRequest;
//import com.coda.efinance.schemas.input.input_13_0.webservice.PostRequest.PostOptions;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.PostRequest.PostOptions;
//import com.coda.efinance.schemas.input.input_13_0.webservice.PostResponse;
import com.coda.efinance.schemas.inputext.input_13_0.webservice.PostResponse;
import com.coda.efinance.schemas.transaction.Header;
import com.coda.efinance.schemas.transaction.Line;
import com.coda.efinance.schemas.transaction.Lines;
import com.coda.efinance.schemas.transaction.Transaction;
import com.coda.efinance.schemas.transaction.TxnKey;

import cz.unit4.coda.CodaDocument;
import cz.unit4.coda.CodaDocumentLine;
import cz.unit4.coda.ListOfCodaDocuments;
import cz.unit4.coda.TransAndDocWideData;
import cz.unit4.service.OneProxy;
import cz.unit4.service.Proxies;


/*
 * This class implements posting to CODA using Web services
 * */

public class CodaDocumentWS {
	private InputServicePortTypes proxy;
	public static Proxies proxies=new Proxies();
	private String lastToken=null;
	
	static final Logger logger = LogManager.getLogger(CodaDocumentWS.class.getName());

	
	/**
	 * Constructor needs URL and URI of the input Web service
	 * @param inputServiceURL
	 * @param inputServiceURI
	 */
	public CodaDocumentWS(String inputServiceURL,String inputServiceURI, String userCODA, String passwordCODA, String cmpcode, String token) throws MalformedURLException{
		//Create proxy for Web services
		//At first lets check if token was added to request	
		boolean createNewConnection = true;
		proxies.ClearOlderProxies();
		if (token !=null && !token.isEmpty()){
			//try to find existing proxy
			if (proxies.getAvailableProxies().get(token)!=null){
				proxy=proxies.getAvailableProxies().get(token).getProxyInput();
				createNewConnection = false;
				lastToken=token;
			}
		}
		if (createNewConnection){
			String strWSDL  = inputServiceURL;
			URL url = new URL(strWSDL);
			QName qname = new QName(inputServiceURI,"InputService","");
			InputService service = new InputService(url,qname);
			
			proxy= service.getInputServicePort();
			
			BindingProvider prov = (BindingProvider)proxy;
			StringBuffer strUserCmp = new StringBuffer(userCODA);
			strUserCmp.append("\\");
			strUserCmp.append(cmpcode);
			prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, strUserCmp.toString());
			prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, passwordCODA);
			
			//And add it sd new to the list of proxies
			String newToken=Proxies.tokenGenerator(16);
			OneProxy oneProxy=new OneProxy(newToken,proxy,null);
			proxies.getAvailableProxies().put(newToken, oneProxy);
			lastToken=newToken;
		}
		
	}//End of constructor
	

	public String getLatsToken(){return this.lastToken;}
	
	/**
	 * This routine does the posting job using UNIT4 Web services
	 * @param documents - object which encapsulates/is built based on JSON representation of the set of CODA documents
	 * @return
	 * @throws JSONException
	 * @throws ParseException 
	 * @throws DatatypeConfigurationException 
	 */
	public JSONObject PostJSONtoCODA(ListOfCodaDocuments documents) throws JSONException, ParseException, DatatypeConfigurationException {
		Header h = new Header();
		
		//Loop the JSON documents
		int numOfDocumentsPosted=0;
		int numOfDocumentsInError=0;
		
		StringBuffer strResponses = new StringBuffer();
		
		String target=documents.getTarget(); //*r*
		
		for (CodaDocument document:documents.getDocuments()){
			//--Header
			TxnKey txnKey = new TxnKey();
			txnKey.setCmpCode(document.getCmpCode());			//cmpcode
			txnKey.setCode(document.getDocCode());				//doccode
			if (document.getDocNum()!=null && !document.getDocNum().equalsIgnoreCase("null")) {
				txnKey.setNumber(document.getDocNum());
			} else {
				txnKey.setNumber("0"); //hopefully document will have the automatic bumbering rule
			}
			h = new Header();
			h.setKey(txnKey);
			h.setCurCode(document.getDocCur());
			
			StringBuffer docDate = new StringBuffer(document.getDocDate());

			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			Date date = df.parse(docDate.toString());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			//GregorianCalendar date - document date - the last day of the period
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			XMLGregorianCalendar documentDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			h.setDate(documentDate);
			h.setPeriod(document.getFinPeriod());

			//Description if exists
			if (document.getDescription()!=null && !document.getDescription().equalsIgnoreCase("null") && !document.getDescription().isEmpty()) {
				h.setDescription(document.getDescription().trim());
			}
			
			//Decide if external references are doc or line wide. If at least one is presented here they are docwide
			boolean areReferencesDocWide = false;
			if (document.getRef1()!=null && !document.getRef1().equalsIgnoreCase("null") && !document.getRef1().isEmpty() ){areReferencesDocWide=true;}
			if (document.getRef2()!=null && !document.getRef2().equalsIgnoreCase("null") && !document.getRef2().isEmpty() ){areReferencesDocWide=true;}
			if (document.getRef3()!=null && !document.getRef3().equalsIgnoreCase("null") && !document.getRef3().isEmpty() ){areReferencesDocWide=true;}
			if (document.getRef4()!=null && !document.getRef4().equalsIgnoreCase("null") && !document.getRef4().isEmpty() ){areReferencesDocWide=true;}
			if (document.getRef5()!=null && !document.getRef5().equalsIgnoreCase("null") && !document.getRef5().isEmpty() ){areReferencesDocWide=true;}
			if (document.getRef6()!=null && !document.getRef6().equalsIgnoreCase("null") && !document.getRef6().isEmpty() ){areReferencesDocWide=true;}
			
			DocumentWideData dwd = new DocumentWideData();
			if (areReferencesDocWide){
				if (document.getRef1()!=null && !document.getRef1().equalsIgnoreCase("null") && !document.getRef1().isEmpty() ){dwd.setExtRef1(document.getRef1());}
				if (document.getRef2()!=null && !document.getRef2().equalsIgnoreCase("null") && !document.getRef2().isEmpty() ){dwd.setExtRef2(document.getRef2());}
				if (document.getRef3()!=null && !document.getRef3().equalsIgnoreCase("null") && !document.getRef3().isEmpty() ){dwd.setExtRef3(document.getRef3());}
				if (document.getRef4()!=null && !document.getRef4().equalsIgnoreCase("null") && !document.getRef4().isEmpty() ){dwd.setExtRef4(document.getRef4());}
				if (document.getRef5()!=null && !document.getRef5().equalsIgnoreCase("null") && !document.getRef5().isEmpty() ){dwd.setExtRef5(document.getRef5());}
				if (document.getRef6()!=null && !document.getRef6().equalsIgnoreCase("null") && !document.getRef6().isEmpty() ){dwd.setExtRef6(document.getRef6());}
			} //references if they are docwide
			
			//Decide wether Due date is document wide and set it if yes
			boolean isDueDateDocWide = false;
			if (document.getDueDate()!=null && !document.getDueDate().equalsIgnoreCase("null") && !document.getDueDate().isEmpty() ){
				isDueDateDocWide =true;
				date = df.parse(document.getDueDate());
				//cal.setTime(date);
				c.setTime(date);
				XMLGregorianCalendar dueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				dwd.setDueDate(dueDate);
			}
			
			boolean isValueDateDocWide = false;
			if (document.getValueDate()!=null && !document.getValueDate().equalsIgnoreCase("null") && !document.getValueDate().isEmpty() ){
				isValueDateDocWide =true;
				date = df.parse(document.getValueDate());
				c.setTime(date);
				XMLGregorianCalendar valueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				dwd.setValueDate(valueDate);
			}
			
			Transaction t = new Transaction();
			t.setHeader(h);
			
			//---Lines
			Lines lines = new Lines(); 
			int numOfDocumentLines=0;
			for (CodaDocumentLine oneLine:document.getLines()){
				numOfDocumentLines++;
				Line l = new Line();
				l.setNumber(numOfDocumentLines);
				//Line type
				if (oneLine.getLineType()!=null && !oneLine.getLineType().equalsIgnoreCase("null") && !oneLine.getLineType().isEmpty()) {
					 switch (Integer.parseInt(oneLine.getLineType())) {
			            case 157:
			            	l.setLineType(TypeCtDocLineTypeB.SUMMARY);	;
			                break;
			            case 158:
			            	l.setLineType(TypeCtDocLineTypeB.ANALYSIS);	;
			                break;
			            case 159:
			            	l.setLineType(TypeCtDocLineTypeB.TAX);	;
			                break;
			            default:    
			            	l.setLineType(TypeCtDocLineTypeB.ANALYSIS);
			            	break;
					 }
				} else {l.setLineType(TypeCtDocLineTypeB.ANALYSIS);}
				
				//Account code
				l.setAccountCode(oneLine.getAccCode());
				
				//Debit or Credit
				if (oneLine.getDebitCredit().equals("161")) //it is passed as string
					l.setLineSense(TypeCtDocLineDrCrB.DEBIT);
				else
					l.setLineSense(TypeCtDocLineDrCrB.CREDIT);
				
				//value - suppose to be separated by comma
				BigDecimal lineValue = new BigDecimal(oneLine.getValueDoc());
				l.setDocValue(lineValue);
				
				if  (oneLine.getValueHome()!=null && !oneLine.getValueHome().equalsIgnoreCase("null") && !oneLine.getValueHome().isEmpty()){
					BigDecimal lineValueHome = new BigDecimal(oneLine.getValueHome());
					l.setHomeValue(lineValueHome);
				}
				
				//Line description if exists
				if (oneLine.getDescription()!=null && !oneLine.getDescription().equalsIgnoreCase("null") && !oneLine.getDescription().isEmpty()) {
					l.setDescription(oneLine.getDescription().trim());
				}
				
				//References if they are LineWide - means request has them on lines
				if (!areReferencesDocWide){
					if(oneLine.getRef1()!=null && !oneLine.getRef1().equalsIgnoreCase("null")  && !oneLine.getRef1().isEmpty()) l.setExtRef1(oneLine.getRef1());
					if(oneLine.getRef2()!=null && !oneLine.getRef2().equalsIgnoreCase("null")  && !oneLine.getRef2().isEmpty()) l.setExtRef2(oneLine.getRef2());
					if(oneLine.getRef3()!=null && !oneLine.getRef3().equalsIgnoreCase("null")  && !oneLine.getRef3().isEmpty()) l.setExtRef3(oneLine.getRef3());
					if(oneLine.getRef4()!=null && !oneLine.getRef4().equalsIgnoreCase("null")  && !oneLine.getRef4().isEmpty()) l.setExtRef4(oneLine.getRef4());
					if(oneLine.getRef5()!=null && !oneLine.getRef5().equalsIgnoreCase("null")  && !oneLine.getRef5().isEmpty()) l.setExtRef5(oneLine.getRef5());
					if(oneLine.getRef6()!=null && !oneLine.getRef6().equalsIgnoreCase("null")  && !oneLine.getRef6().isEmpty()) l.setExtRef6(oneLine.getRef6());
				}
				
				//Due date if it is LineWide
				if(!isDueDateDocWide){
					if (oneLine.getDueDate()!=null && !oneLine.getDueDate().equalsIgnoreCase("null") && !oneLine.getDueDate().isEmpty()) {
						date = df.parse(oneLine.getDueDate());
						c.setTime(date);
						XMLGregorianCalendar dueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
						l.setDueDate(dueDate);
					}
				}//end if !isDueDateDocWide
				
				//Value date if it is LineWide
				if(!isValueDateDocWide){
					if (oneLine.getValueDate()!=null && !oneLine.getValueDate().equalsIgnoreCase("null") && !oneLine.getValueDate().isEmpty()) {
						date = df.parse(oneLine.getValueDate());
						c.setTime(date);
						XMLGregorianCalendar valueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
						l.setValueDate(valueDate);
					}
				}//end if !isDueDateDocWide
				//Add the line
				lines.getLines().add(l);
				
			}//Loop of the one's document (of my CodaDocument type) lines
			
			//Set Transaction
			t.setLines(lines);
			PostRequest inputReq = new PostRequest();
			inputReq.setTransaction(t);
			
			//Set Post options
			PostOptions popt = new PostOptions();
			if (target.equalsIgnoreCase("books")){
				popt.setPostto(TypeCtDocDest.BOOKS);
			} else {
				popt.setPostto(TypeCtDocDest.INTRAY);
			}
			inputReq.setPostOptions(popt);
			
			//set Document Wide data if any. dwd is prepared above in document header section 
			if (areReferencesDocWide  || isDueDateDocWide || isValueDateDocWide){
				InputPostDocument ipd = new InputPostDocument();
				ipd.setDocumentWideData(dwd);
				inputReq.setPostData(ipd);
			}
			
			try {
				PostResponse resp = proxy.post(inputReq);
				logger.debug(String.format( "Document %s-%s-%s posted.", resp.getKey().getCmpCode().toString(),resp.getKey().getCode().toString(),resp.getKey().getNumber().toString() ));
				numOfDocumentsPosted++;
				strResponses.append("<postDocumentResponse>");
				strResponses.append(String.format( "%s/%s/%s", resp.getKey().getCmpCode().toString(),resp.getKey().getCode().toString(),resp.getKey().getNumber().toString()));
				strResponses.append("</postDocumentResponse>");
			} catch (Exception e){
				numOfDocumentsInError++;
				strResponses.append("<postDocumentResponseError>");
				strResponses.append(e.getMessage());
				strResponses.append("</postDocumentResponseError>");
			}
		}// loop of documents passed by variable 'documents' (of ListOfCodaDocuments type)
		logger.debug(String.format( "Number of documents posted:%d",numOfDocumentsPosted ));
		strResponses.append("<numOfPosted>");
		strResponses.append(String.format( "%d",numOfDocumentsPosted ));
		strResponses.append("</numOfPosted>");
		strResponses.append("<numInError>");
		strResponses.append(String.format( "%d",numOfDocumentsInError));
		strResponses.append("</numInError>");
		
		
		JSONObject xmlJSONObj = XML.toJSONObject(strResponses.toString());
        return xmlJSONObj;
	} //End of PostJSONtoCODA
	
	
	/**
	 * UpdateJSONtoCODA updates existing documents in Intray
	 * @param documents - I suppose it will be possible to open the document, open it and to change
	 * only the provided fields. If there is more lines entered than they are added.
	 * Sample of manipulating the document in Intray is in the sample project TestCodaWs
	 * @return
	 * @throws JSONException
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	public JSONObject UpdateJSONtoCODA(ListOfCodaDocuments documents) throws JSONException, ParseException, DatatypeConfigurationException, Exception {
		//Loop the JSON documents
		int numOfDocumentsPosted=0;
		StringBuffer strResponses = new StringBuffer();
		
		String target=documents.getTarget(); //*r*
		
		for (CodaDocument document:documents.getDocuments()){
			//for each document try to open it first - receiving the Transaction
			
			try {
				//reading existing data in Coda, both transaction and document wide data
				TransAndDocWideData existingTransAndDocWideData = OpenExistingDocument(document.getCmpCode(),document.getDocCode(),document.getDocNum(),document.getTemplate());
				
				//Merge existing data with updates;
				TransAndDocWideData modifiedTransAndDocWideData =MergeTransactions(document, existingTransAndDocWideData);

				PostRequest inputReq = new PostRequest();
				inputReq.setTransaction(modifiedTransAndDocWideData.getCodaTransaction());
				
				//We will need also to deal with document wide data....
				if (existingTransAndDocWideData.areReferencesDocWide() || existingTransAndDocWideData.isDueDateDocWide() || existingTransAndDocWideData.isValueDateDocWide()){
					InputPostDocument ipd = new InputPostDocument();
					ipd.setDocumentWideData(modifiedTransAndDocWideData.getDocumentWideData());
					inputReq.setPostData(ipd);
				}
				
				
				//Set Post options
				PostOptions popt = new PostOptions();
				if (target.equalsIgnoreCase("books")){
					popt.setPostto(TypeCtDocDest.BOOKS);
				} else {
					popt.setPostto(TypeCtDocDest.INTRAY);
				}
				inputReq.setPostOptions(popt);
				
				PostResponse resp = proxy.post(inputReq);
				
				strResponses.append("<updateDocumentResponse>");
				strResponses.append(String.format( "%s/%s/%s", resp.getKey().getCmpCode().toString(),resp.getKey().getCode().toString(),resp.getKey().getNumber().toString()));
				strResponses.append("</updateDocumentResponse>");

			} catch (Exception e) {
				strResponses.append("<updateDocumentResponseError>");
				strResponses.append(e.getLocalizedMessage());
				strResponses.append("</updateDocumentResponseError>");
			} //end of block trying t open document
			
		}//end of the loop of documents
		 logger.debug(String.format( "Number of documents updated:%d",numOfDocumentsPosted ));
         JSONObject xmlJSONObj = XML.toJSONObject(strResponses.toString());
         return xmlJSONObj;
	} //end of UpdateJSONtoCODA
	

	/**
	 * Helper fuction which returns one documet as transaction if it exists in Coda
	 * @param cmpcode
	 * @param doccode
	 * @param docnum
	 * @param template
	 * @return
	 * @throws Exception if not possible to Open document in Intray
	 */
	private TransAndDocWideData OpenExistingDocument(String cmpcode,String doccode, String docnum, String template) throws Exception {
		TxnKey txnk = new TxnKey();
		txnk.setCmpCode(cmpcode);
		txnk.setCode(doccode);
		txnk.setNumber(docnum);
		IntrayLoadRequest iLReq = new IntrayLoadRequest();
		iLReq.setKey(txnk);
		iLReq.setTemplateCode(template);
		IntrayLoadResponse ilResp = proxy.intrayLoad(iLReq);
		
		
		TransAndDocWideData dataToReturn = new TransAndDocWideData(ilResp.getTransaction(),ilResp.getPostData().getDocumentWideData()) ;
		
		/* This is to decide if there are any document wide data in existing CODA document.
		 * We will not use it now, because this decision will be done based on data on JSON input
		if (ilResp.getPostData().getDocumentWideData()!=null){
			dataToReturn.setAreReferencesDocWide(false);
			if (ilResp.getPostData().getDocumentWideData().getExtRef1()!=null && !ilResp.getPostData().getDocumentWideData().getExtRef1().equalsIgnoreCase("null") && !ilResp.getPostData().getDocumentWideData().getExtRef1().isEmpty()){dataToReturn.setAreReferencesDocWide(true);}
			if (ilResp.getPostData().getDocumentWideData().getExtRef2()!=null && !ilResp.getPostData().getDocumentWideData().getExtRef2().equalsIgnoreCase("null") && !ilResp.getPostData().getDocumentWideData().getExtRef2().isEmpty()){dataToReturn.setAreReferencesDocWide(true);}
			if (ilResp.getPostData().getDocumentWideData().getExtRef3()!=null && !ilResp.getPostData().getDocumentWideData().getExtRef3().equalsIgnoreCase("null") && !ilResp.getPostData().getDocumentWideData().getExtRef3().isEmpty()){dataToReturn.setAreReferencesDocWide(true);}
			if (ilResp.getPostData().getDocumentWideData().getExtRef4()!=null && !ilResp.getPostData().getDocumentWideData().getExtRef4().equalsIgnoreCase("null") && !ilResp.getPostData().getDocumentWideData().getExtRef4().isEmpty()){dataToReturn.setAreReferencesDocWide(true);}
			if (ilResp.getPostData().getDocumentWideData().getExtRef5()!=null && !ilResp.getPostData().getDocumentWideData().getExtRef5().equalsIgnoreCase("null") && !ilResp.getPostData().getDocumentWideData().getExtRef5().isEmpty()){dataToReturn.setAreReferencesDocWide(true);}
			if (ilResp.getPostData().getDocumentWideData().getExtRef6()!=null && !ilResp.getPostData().getDocumentWideData().getExtRef6().equalsIgnoreCase("null") && !ilResp.getPostData().getDocumentWideData().getExtRef6().isEmpty()){dataToReturn.setAreReferencesDocWide(true);}
			
			if (ilResp.getPostData().getDocumentWideData().getDueDate()!=null) {dataToReturn.setIsDueDateDocWide(true);}
			if (ilResp.getPostData().getDocumentWideData().getValueDate()!=null) {dataToReturn.setIsValueDateDocWide(true);}
		}*/
		
		
		
		return dataToReturn ;
	} //OpenExistingDocument
	
	
	/**
	 * Write changes from input(updatedDocument) to existing CODA document (existingTransAndWideData)
	 * Fields are updated. the new lines added if any
	 * @param updatedDocument
	 * @param existingTransAndWideData
	 * @return
	 * @throws JSONException
	 * @throws ParseException
	 * @throws Exception
	 */
	
	private TransAndDocWideData MergeTransactions(CodaDocument updatedDocument, TransAndDocWideData existingTransAndWideData) throws JSONException, ParseException, Exception{
		//open updatedDocument
		//HEADER
		
		TransAndDocWideData returnedTransAndWideData = existingTransAndWideData; //make it the same on the beginning
		//also wew ill need Transaction data from it	
		Transaction existingDocument = existingTransAndWideData.getCodaTransaction();
		
		//We will not change cmpcode,doccode,docnum. so we start with docdate
		//Curcode);
		
		if (updatedDocument.getDocCur()!=null && !updatedDocument.getDocCur().isEmpty() && updatedDocument.getDocCur().equals("null")){
			existingDocument.getHeader().setCurCode(updatedDocument.getDocCur());
		}
		//Docdate
		if (updatedDocument.getDocDate()!=null && !updatedDocument.getDocDate().isEmpty() && updatedDocument.getDocDate().equals("null")){
			StringBuffer docDate = new StringBuffer(updatedDocument.getDocDate());
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			Date date = df.parse(docDate.toString());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			XMLGregorianCalendar documentDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			existingDocument.getHeader().setDate(documentDate);
		}

		//Financial period
		if (updatedDocument.getFinPeriod()!=null && !updatedDocument.getFinPeriod().isEmpty() && updatedDocument.getFinPeriod().equals("null")){
			existingDocument.getHeader().setPeriod(updatedDocument.getFinPeriod());
		}
		
		//Description if exists
		if (updatedDocument.getDescription()!=null && !updatedDocument.getDescription().equalsIgnoreCase("null") && !updatedDocument.getDescription().isEmpty()) {
			existingDocument.getHeader().setDescription(updatedDocument.getDescription().trim());
		}
		
		//Decide if external references are doc or line wide. If at least one is presented here they are docwide
		boolean areReferencesDocWide = false;
		if (updatedDocument.getRef1()!=null && !updatedDocument.getRef1().equalsIgnoreCase("null") && !updatedDocument.getRef1().isEmpty() ){areReferencesDocWide=true;}
		if (updatedDocument.getRef2()!=null && !updatedDocument.getRef2().equalsIgnoreCase("null") && !updatedDocument.getRef2().isEmpty() ){areReferencesDocWide=true;}
		if (updatedDocument.getRef3()!=null && !updatedDocument.getRef3().equalsIgnoreCase("null") && !updatedDocument.getRef3().isEmpty() ){areReferencesDocWide=true;}
		if (updatedDocument.getRef4()!=null && !updatedDocument.getRef4().equalsIgnoreCase("null") && !updatedDocument.getRef4().isEmpty() ){areReferencesDocWide=true;}
		if (updatedDocument.getRef5()!=null && !updatedDocument.getRef5().equalsIgnoreCase("null") && !updatedDocument.getRef5().isEmpty() ){areReferencesDocWide=true;}
		if (updatedDocument.getRef6()!=null && !updatedDocument.getRef6().equalsIgnoreCase("null") && !updatedDocument.getRef6().isEmpty() ){areReferencesDocWide=true;}
		
		DocumentWideData dwd = new DocumentWideData();
		if (areReferencesDocWide){
			returnedTransAndWideData.setAreReferencesDocWide(true);
			if (updatedDocument.getRef1()!=null && !updatedDocument.getRef1().equalsIgnoreCase("null") && !updatedDocument.getRef1().isEmpty() ){dwd.setExtRef1(updatedDocument.getRef1());}
			if (updatedDocument.getRef2()!=null && !updatedDocument.getRef2().equalsIgnoreCase("null") && !updatedDocument.getRef2().isEmpty() ){dwd.setExtRef2(updatedDocument.getRef2());}
			if (updatedDocument.getRef3()!=null && !updatedDocument.getRef3().equalsIgnoreCase("null") && !updatedDocument.getRef3().isEmpty() ){dwd.setExtRef3(updatedDocument.getRef3());}
			if (updatedDocument.getRef4()!=null && !updatedDocument.getRef4().equalsIgnoreCase("null") && !updatedDocument.getRef4().isEmpty() ){dwd.setExtRef4(updatedDocument.getRef4());}
			if (updatedDocument.getRef5()!=null && !updatedDocument.getRef5().equalsIgnoreCase("null") && !updatedDocument.getRef5().isEmpty() ){dwd.setExtRef5(updatedDocument.getRef5());}
			if (updatedDocument.getRef6()!=null && !updatedDocument.getRef6().equalsIgnoreCase("null") && !updatedDocument.getRef6().isEmpty() ){dwd.setExtRef6(updatedDocument.getRef6());}
		} //references if they are docwide
		
		//Decide whether Due date is document wide and set it if yes
		boolean isDueDateDocWide = false;
		if (updatedDocument.getDueDate()!=null && !updatedDocument.getDueDate().equalsIgnoreCase("null") && !updatedDocument.getDueDate().isEmpty() ){
			isDueDateDocWide =true;
			returnedTransAndWideData.setIsDueDateDocWide(true);
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			Date date = df.parse(updatedDocument.getDueDate());
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			XMLGregorianCalendar dueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			dwd.setDueDate(dueDate);
		}
		
		boolean isValueDateDocWide = false;
		if (updatedDocument.getValueDate()!=null && !updatedDocument.getValueDate().equalsIgnoreCase("null") && !updatedDocument.getValueDate().isEmpty() ){
			isValueDateDocWide =true;
			returnedTransAndWideData.setIsValueDateDocWide(true);
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			Date date = df.parse(updatedDocument.getValueDate());
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			XMLGregorianCalendar valueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			dwd.setValueDate(valueDate);
		}
	
		//Now modifications/adding of the lines
		//Loop of updatedDocument lines (they are those lines from JSON request)
		for(CodaDocumentLine updl: updatedDocument.getLines()){  //updatedDocument is OUR new data from JSON
			Line updatedLine = null;

			boolean addLine = false;
			//if lineNum is provided, try to  update
			if (updl.getLineNum()!=null && !updl.getLineNum().equalsIgnoreCase("null") && !updl.getLineNum().isEmpty()){
				//check if it has not bigger number than existing lines
				if ( (Integer.parseInt(updl.getLineNum())-1) <= existingDocument.getLines().getLines().size() ){
					//it is rally the existing line
					
					int lineNum = Integer.parseInt(updl.getLineNum());
					updatedLine = UpdateOrAddLine(lineNum,updl,existingDocument.getLines().getLines().get(Integer.parseInt(updl.getLineNum())-1),areReferencesDocWide,  isDueDateDocWide, isValueDateDocWide);
					if (updatedLine!=null) existingDocument.getLines().getLines().set((Integer.parseInt(updl.getLineNum())-1),updatedLine);
				}  else {
					addLine = true; //line number is bigger than existing one
				}
			} else {
				addLine = true;  //line num was not provided
			}
			
			if (addLine){
				int lineNum = existingDocument.getLines().getLines().size() + 1;
				updatedLine = UpdateOrAddLine(lineNum, updl,null,areReferencesDocWide,  isDueDateDocWide, isValueDateDocWide); //if it is adding we do not need existing line, so the second parameter is null
				if (updatedLine!=null) existingDocument.getLines().getLines().add(updatedLine);
			}// if add line
		}//end of updatedDocument lines
		
		returnedTransAndWideData.setCodaTransaction(existingDocument);
		
		//existingTransAndWideData.setCodaTransaction(existingDocument);
		if (dwd!=null) returnedTransAndWideData.setCodaDocWideData(dwd);
		
	    return returnedTransAndWideData; 
	    
	}//End of MergeTransactions
	
	
	/**
	 * Goes through the line fields updating the existing fileds by data provided in JSON request.
	 * If l (Line) is null than it is adding the new line
	 * @param updatedLineData
	 * @param l
	 * @param areReferencesDocWide
	 * @param isDueDateDocWide
	 * @param isValueDateDocWide
	 * @return
	 */
	private Line UpdateOrAddLine(int lineNum,CodaDocumentLine updatedLineData, Line l,boolean areReferencesDocWide,boolean isDueDateDocWide,boolean isValueDateDocWide) throws Exception{ //
		Line returnedLine = null;
		if (l != null) {
			returnedLine = l; //we will use the existing as beginning for modification
		} else {
			returnedLine = new Line();  //we will add all data as the new
		}
			
		//Line number
		returnedLine.setNumber(lineNum);
		//Line type
		if (updatedLineData.getLineType()!=null && !updatedLineData.getLineType().equalsIgnoreCase("null") && !updatedLineData.getLineType().isEmpty()) {
			 switch (Integer.parseInt(updatedLineData.getLineType())) {
	            case 157:
	            	returnedLine.setLineType(TypeCtDocLineTypeB.SUMMARY);	;
	                break;
	            case 158:
	            	returnedLine.setLineType(TypeCtDocLineTypeB.ANALYSIS);	;
	                break;
	            case 159:
	            	returnedLine.setLineType(TypeCtDocLineTypeB.TAX);	;
	                break;
	            default:    
	            	returnedLine.setLineType(TypeCtDocLineTypeB.ANALYSIS);
	            	break;
			 }
		} else {returnedLine.setLineType(TypeCtDocLineTypeB.ANALYSIS);}
		
		
		//Account code
		if (updatedLineData.getAccCode()!=null && !updatedLineData.getAccCode().equalsIgnoreCase("null") && !updatedLineData.getAccCode().isEmpty()) {
			returnedLine.setAccountCode(updatedLineData.getAccCode());
		}

		//Debit or Credit
		if (updatedLineData.getDebitCredit()!=null && !updatedLineData.getDebitCredit().equalsIgnoreCase("null") && !updatedLineData.getDebitCredit().isEmpty()) {
			if (updatedLineData.getDebitCredit().equals("161")) //it is passed as string
				returnedLine.setLineSense(TypeCtDocLineDrCrB.DEBIT);
			else
				returnedLine.setLineSense(TypeCtDocLineDrCrB.CREDIT);
		}
	
		//value doc 
		if (updatedLineData.getValueDoc()!=null && !updatedLineData.getValueDoc().equalsIgnoreCase("null") && !updatedLineData.getValueDoc().isEmpty()) {
			BigDecimal lineValue = new BigDecimal(updatedLineData.getValueDoc());
			returnedLine.setDocValue(lineValue);
		}
		//value home
		if  (updatedLineData.getValueHome()!=null && !updatedLineData.getValueHome().equalsIgnoreCase("null") && !updatedLineData.getValueHome().isEmpty()){
			BigDecimal lineValueHome = new BigDecimal(updatedLineData.getValueHome());
			returnedLine.setHomeValue(lineValueHome);
		}
			
		//Line description if exists
		if (updatedLineData.getDescription()!=null && !updatedLineData.getDescription().equalsIgnoreCase("null") && !updatedLineData.getDescription().isEmpty()) {
			returnedLine.setDescription(updatedLineData.getDescription().trim());
		}
	

		//References if they are LineWide - means request has them on lines
		if (!areReferencesDocWide){
			if(updatedLineData.getRef1()!=null && !updatedLineData.getRef1().equalsIgnoreCase("null")  && !updatedLineData.getRef1().isEmpty()) returnedLine.setExtRef1(updatedLineData.getRef1());
			if(updatedLineData.getRef2()!=null && !updatedLineData.getRef2().equalsIgnoreCase("null")  && !updatedLineData.getRef2().isEmpty()) returnedLine.setExtRef2(updatedLineData.getRef2());
			if(updatedLineData.getRef3()!=null && !updatedLineData.getRef3().equalsIgnoreCase("null")  && !updatedLineData.getRef3().isEmpty()) returnedLine.setExtRef3(updatedLineData.getRef3());
			if(updatedLineData.getRef4()!=null && !updatedLineData.getRef4().equalsIgnoreCase("null")  && !updatedLineData.getRef4().isEmpty()) returnedLine.setExtRef4(updatedLineData.getRef4());
			if(updatedLineData.getRef5()!=null && !updatedLineData.getRef5().equalsIgnoreCase("null")  && !updatedLineData.getRef5().isEmpty()) returnedLine.setExtRef5(updatedLineData.getRef5());
			if(updatedLineData.getRef6()!=null && !updatedLineData.getRef6().equalsIgnoreCase("null")  && !updatedLineData.getRef6().isEmpty()) returnedLine.setExtRef6(updatedLineData.getRef6());
		}
	
		//Due date if it is LineWide
		if(!isDueDateDocWide){
			if (updatedLineData.getDueDate()!=null && !updatedLineData.getDueDate().equalsIgnoreCase("null") && !updatedLineData.getDueDate().isEmpty()) {
				DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
				Date date = df.parse(updatedLineData.getDueDate());
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(date);
				XMLGregorianCalendar dueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				returnedLine.setDueDate(dueDate);
			}
		}//end if !isDueDateDocWide
		
		//Value date if it is LineWide
		if(!isValueDateDocWide){
			if (updatedLineData.getValueDate()!=null && !updatedLineData.getValueDate().equalsIgnoreCase("null") && !updatedLineData.getValueDate().isEmpty()) {
				DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
				Date date = df.parse(updatedLineData.getValueDate());
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(date);
				XMLGregorianCalendar valueDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				returnedLine.setValueDate(valueDate);
			}
		}//end if !isDueDateDocWide
		
		return returnedLine;
	}//end of MergeLines
	
	
	
} //end of class
