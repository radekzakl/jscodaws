package cz.unit4.codaws;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.coda.efinance.schemas.transaction.ChkAccCodeAnswer;
import com.coda.efinance.schemas.transaction.ChkAccCodeData;
import com.coda.efinance.schemas.transaction.ChkAccCodeFailed;
import com.coda.efinance.schemas.transaction.account_12_0.webservice.AccountService;
import com.coda.efinance.schemas.transaction.account_12_0.webservice.AccountServicePortTypes;
import com.coda.efinance.schemas.transaction.account_12_0.webservice.CheckCodeRequest;
import com.coda.efinance.schemas.transaction.account_12_0.webservice.CheckCodeResponse;

import cz.unit4.coda.CodaAccount;
import cz.unit4.service.OneProxy;
import cz.unit4.service.Proxies;

public class CodaAccountWS {
	
	private AccountServicePortTypes proxy;
	public static Proxies proxies = new Proxies();
	private String lastToken=null;
	
	//private static final Logger logger = Logger.getLogger(CodaDocumentWS.class);
	static final Logger logger = LogManager.getLogger(CodaAccountWS.class.getName());
	
	public CodaAccountWS (String accountServiceURL,String accountServiceURI, String userCODA, String passwordCODA, String cmpcode, String token) throws MalformedURLException{
		//Create proxy for Web services
		
		boolean createNewConnection = true;
		if (token !=null && !token.isEmpty()){
			//try to find existing proxy
			proxies.ClearOlderProxies();
			if (proxies.getAvailableProxies().get(token)!=null){
				proxy=proxies.getAvailableProxies().get(token).getProxyAccount();
				createNewConnection = false;
				lastToken=token;
			}
		}
		if (createNewConnection){
		
			String strWSDL  = accountServiceURL;
			URL url = new URL(strWSDL);
			QName qname = new QName(accountServiceURI,"AccountService","");
			AccountService service = new AccountService(url,qname);
			
			proxy= service.getAccountServicePort();
			
			BindingProvider prov = (BindingProvider)proxy;
			StringBuffer strUserCmp = new StringBuffer(userCODA);
			strUserCmp.append("\\");
			strUserCmp.append(cmpcode);
			prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, strUserCmp.toString());
			prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, passwordCODA);
			
			//And add it sd new to the list of proxies
			String newToken=Proxies.tokenGenerator(16);
			OneProxy oneProxy=new OneProxy(newToken,null,proxy);
			proxies.getAvailableProxies().put(newToken, oneProxy);
			lastToken=newToken;
		}
		
	
	}//End of constructor
	
	
	public String getLatsToken(){return this.lastToken;}
	
	public JSONObject ValidateAccountInCODA(CodaAccount account) throws JSONException, ParseException, DatatypeConfigurationException {
		StringBuffer strResponses = new StringBuffer();
		
		try {
			CheckCodeRequest codeRequest = new  CheckCodeRequest();
			ChkAccCodeData codeData= new ChkAccCodeData();
			codeData.setCmpCode(account.getCmpCode());
			codeData.setAccountCode(account.getAccountCode());
			codeRequest.setChkCodeData(codeData);

			CheckCodeResponse resp = proxy.checkCode(codeRequest);
			ChkAccCodeAnswer ansv= resp.getAnswer();
			
			ChkAccCodeFailed failed = ansv.getFailed();
			if (ansv.isGood()) {
				//account is OK
				strResponses.append("<validateAccountResponse>");
				strResponses.append("<status>");
				strResponses.append(String.format( "%d", 0));
				strResponses.append("</status>");
				strResponses.append("</validateAccountResponse>");
			} else {
				String reason = failed.getReason().getTexts().get(0).getValue();
				strResponses.append("<validateAccountResponse>");
				strResponses.append("<status>");
				strResponses.append(String.format( "%d", -1));
				strResponses.append("</status>");
				strResponses.append("<message>");
				strResponses.append(String.format( "%s", reason));
				strResponses.append("</message>");
				strResponses.append("</validateAccountResponse>");
			}
			//this contain various information about elements e.g. names
			//ChkAccCodeResult result= ansv.getPassed();
		} catch (Exception e){
			strResponses.append("<validateAccountError>");
			strResponses.append(e.getMessage());
			strResponses.append("</validateAccountError>");
		}

         JSONObject xmlJSONObj = XML.toJSONObject(strResponses.toString());
         return xmlJSONObj;
	} //End of ValidateAccountInCODA
}
