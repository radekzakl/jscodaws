package cz.unit4.coda;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


@XmlRootElement
public class CodaAccount {
	private String cmpCode=null;
	private String accountCode=null;
	private String token=null;
	
	public void setToken(String token) {this.token=token;}
	public String getToken(){return token;}
	
	public void setCmpCode(String cmpCode) {this.cmpCode=cmpCode;}
	public String getCmpCode(){return cmpCode;}
	
	public void setAccountCode(String accountCode) {this.accountCode=accountCode;}
	public String getAccountCode(){return accountCode;}
	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(this);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonInString;
	} //End of toString
	
	
	/**
	 * Sample constructor
	 */
	public CodaAccount(){
		cmpCode="TEST";
		accountCode="321001.DOD01";
	}

	
	
	
}
