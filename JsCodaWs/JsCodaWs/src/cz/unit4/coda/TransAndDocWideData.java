package cz.unit4.coda;
import com.coda.efinance.schemas.transaction.Transaction;
import com.coda.efinance.schemas.inputext.DocumentWideData;

/**
 * Combines CODA Transaction and CODA DocumentWideData in one Object
 * Needed for the update of the existing documents in Intray
 * @author RZakl
 *
 */
public class TransAndDocWideData {
	private Transaction codaTransaction=null;
	private DocumentWideData codaDocWideData=null;
	private boolean areReferencesDocWide=false;
	private boolean isDueDateDocWide=false;
	private boolean isValueDateDocWide=false;
	
	public void setAreReferencesDocWide(boolean areReferencesDocWide){
		this.areReferencesDocWide=areReferencesDocWide;
	}
	public boolean areReferencesDocWide(){return areReferencesDocWide;}
	
	public void setIsDueDateDocWide(boolean isDueDateDocWide){
		this.isDueDateDocWide = isDueDateDocWide;
	}
	public boolean isDueDateDocWide(){return this.isDueDateDocWide;}
	
	public void setIsValueDateDocWide(boolean isValueDateDocWide){
		this.isValueDateDocWide = isValueDateDocWide;
	}
	public boolean isValueDateDocWide(){return this.isValueDateDocWide;}
	
	
	
	public void setCodaTransaction(Transaction transaction){
		this.codaTransaction=transaction;
	}
	public Transaction getCodaTransaction(){return this.codaTransaction;}
	
	
	public void setCodaDocWideData(DocumentWideData  documentWideData){
		this.codaDocWideData=documentWideData;
	}
	
	public DocumentWideData getDocumentWideData(){
		return this.codaDocWideData;
	}
	
	public TransAndDocWideData(Transaction codaTransaction,DocumentWideData codaDocWideData){
		if (codaTransaction!=null) {this.codaTransaction=codaTransaction;}
		if (codaDocWideData!=null) {this.codaDocWideData=codaDocWideData;}
	}
	
}
