package cz.unit4.coda;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@XmlRootElement
public class ListOfCodaDocuments {
	private ArrayList<CodaDocument> documents = new ArrayList<>();
	private String target=null; //*r*
	private String token=null;
	
	
	public void setToken(String token) {this.token=token;}
	public String getToken(){return token;}
	
	public void setDocuments(ArrayList<CodaDocument> documents){
		this.documents = documents;
	}

	public ArrayList<CodaDocument> getDocuments(){return this.documents;}
	
	//*r*
	public void setTarget(String target){
		this.target=target;
	}
	public String getTarget(){
		return this.target;
	}
	
	//*r*
	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(this);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonInString;
	} //End of toString
	
	//Some constructor
	public ListOfCodaDocuments(){
			
		documents.add(new CodaDocument());
	}
	
}
