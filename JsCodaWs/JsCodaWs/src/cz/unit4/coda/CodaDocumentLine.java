package cz.unit4.coda;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class CodaDocumentLine holds data of one line for the document which should be posted/modified in CODA
 * CodaDocument holds the list of lines as  private ArrayList<CodaDocumentLine> lines
 * 
 * The original type for VB6 had some other properties, which are NOT implemented in this version
 * Status, Qty, QtyElLevel, QtyPosition
 * 
 * @author RZakl
 * @date 18.9.2017
 *
 */
@XmlRootElement
public class CodaDocumentLine {
	private String lineNum=null;
	private String lineType=null; //157, 158, 159
	private String valueDoc=null;
	private String valueHome=null;
	private String accCode=null;
	private String debitCredit=null; //160 credit, 161 debit
	private String userStat=null;
	private String ref1=null;
	private String ref2=null;
	private String ref3=null;
	private String ref4=null;
	private String ref5=null;
	private String ref6=null;
	private String dueDate=null;
	private String valueDate=null;
	private String description=null;
	//Tax details
	private String sumOfTaxValue = null;
	private String lineTaxcode=null;
	private String lineTaxvalue=null;
	private String taxTaxCode=null;
	private String taxTaxTurnover=null;
	
	
	//getTers and setTers
	
	//linenum will be probably needed only in case of update
	public void setLineNum(String lineNum) {this.lineNum=lineNum;}
	public String getLineNum(){return lineNum;}
		
	public void setLineType(String lineType) {this.lineType=lineType;}
	public String getLineType(){return lineType;}
	
	public void setValueDoc(String valueDoc) {this.valueDoc=valueDoc;}
	public String getValueDoc(){return valueDoc;}
	
	public void setValueHome(String valueHome) {this.valueHome=valueHome;}
	public String getValueHome(){return valueHome;}
	
	
	public void setAccCode(String accCode) {this.accCode=accCode;}
	public String getAccCode(){return accCode;}

	public void setDebitCredit(String debitCredit) {this.debitCredit=debitCredit;}
	public String getDebitCredit(){return debitCredit;}
	
	public void setUserStat(String userStat) {this.userStat=userStat;}
	public String getUserStat(){return userStat;}
	
	public void setRef1(String ref1) {this.ref1=ref1;}
	public String getRef1(){return ref1;}
	
	public void setRef2(String ref2) {this.ref2=ref2;}
	public String getRef2(){return ref2;}

	public void setRef3(String ref3) {this.ref3=ref3;}
	public String getRef3(){return ref3;}
	
	public void setRef4(String ref4) {this.ref4=ref4;}
	public String getRef4(){return ref4;}

	public void setRef5(String ref5) {this.ref5=ref5;}
	public String getRef5(){return ref5;}
	
	public void setRef6(String ref6) {this.ref6=ref6;}
	public String getRef6(){return ref6;}
	
	public void setDueDate(String dueDate) {this.dueDate=dueDate;}
	public String getDueDate(){return dueDate;}
	
	public void setValueDate(String valueDate) {this.valueDate=valueDate;}
	public String getValueDate(){return valueDate;}
	
	
	public void setDescription(String description) {this.description=description;}
	public String getDescription(){return description;}
	
	//SumaofTaxValue
	public void setSumOfTaxValue(String sumOfTaxValue) {this.sumOfTaxValue=sumOfTaxValue;}
	public String getSumOfTaxValue(){return sumOfTaxValue;}

	//LineTaxcode
	public void setLineTaxcode(String lineTaxcode) {this.lineTaxcode=lineTaxcode;}
	public String getLineTaxcode(){return lineTaxcode;}
	
	//LineTaxvalue
	public void setLineTaxvalue(String lineTaxvalue) {this.lineTaxvalue=lineTaxvalue;}
	public String getLineTaxvalue(){return lineTaxvalue;}
	
	//TaxTaxCode
	public void setTaxTaxCode(String taxTaxCode) {this.taxTaxCode=taxTaxCode;}
	public String getTaxTaxCode(){return taxTaxCode;}
	
	//TaxTaxTurnover
	public void setTaxTaxTurnover(String taxTaxTurnover) {this.taxTaxTurnover=taxTaxTurnover;}
	public String getTaxTaxTurnover(){return taxTaxTurnover;}
	
	
	CodaDocumentLine(){
		this.lineType="157";
	}

}

