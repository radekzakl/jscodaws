package cz.unit4.coda;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CodaCompany {
	private String cmpCode=null;
	public void setCmpCode(String cmpCode) {this.cmpCode=cmpCode;}
	public String getCmpCode(){return cmpCode;}
	
	public CodaCompany(){
		cmpCode="SOMETHING";
		
	}
	
}
