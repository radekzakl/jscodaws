package cz.unit4.coda;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Class CodaDocument holds data needed for one document which should be posted/modified in CODA
 * Represents Document in JSON form and as such is transferred by HTTP protol POST method from the client
 * @author RZakl
 * @date 14.9.2017
 * try to remove @XmlRootElement
 */

@XmlRootElement
public class CodaDocument {
	//now just test
	private String cmpCode=null;
	private String docCode=null;
	private String docNum=null;
	private String docCur=null;
	private String ref1=null;
	private String ref2=null;
	private String ref3=null;
	private String ref4=null;
	private String ref5=null;
	private String ref6=null;
	private String description=null;
	private String docDate=null;
	private String dueDate=null;
	private String valueDate=null;
	private String finPeriod=null;
	private String template=null;

	private ArrayList<CodaDocumentLine> lines = new ArrayList<>();
	
	public void setCmpCode(String cmpCode) {this.cmpCode=cmpCode;}
	public String getCmpCode(){return cmpCode;}

	public void setDocCode(String docCode) {this.docCode=docCode;}
	public String getDocCode(){return docCode;}
	
	public void setDocNum(String docNum) {this.docNum=docNum;}
	public String getDocNum(){return docNum;}

	public void setTemplate(String template) {this.template=template;}
	public String getTemplate(){return template;}

	
	
	public void setDocCur(String docCur) {this.docCur=docCur;}
	public String getDocCur(){return docCur;}
	
	public void setRef1(String ref1) {this.ref1=ref1;}
	public String getRef1(){return ref1;}
	
	public void setRef2(String ref2) {this.ref2=ref2;}
	public String getRef2(){return ref2;}

	public void setRef3(String ref3) {this.ref3=ref3;}
	public String getRef3(){return ref3;}
	
	public void setRef4(String ref4) {this.ref4=ref4;}
	public String getRef4(){return ref4;}

	public void setRef5(String ref5) {this.ref5=ref5;}
	public String getRef5(){return ref5;}
	
	public void setRef6(String ref6) {this.ref6=ref6;}
	public String getRef6(){return ref6;}

	public void setDescription(String description) {this.description=description;}
	public String getDescription(){return description;}
	
	public void setDocDate(String docDate) {this.docDate=docDate;}
	public String getDocDate(){return docDate;}
	
	
	public void setValueDate(String valueDate) {this.valueDate=valueDate;}
	public String getValueDate(){return valueDate;}
	
	public void setDueDate(String dueDate) {this.dueDate=dueDate;}
	public String getDueDate(){return dueDate;}
	
	public void setFinPeriod(String finPeriod) {this.finPeriod=finPeriod;}
	public String getFinPeriod(){return finPeriod;}

	public void setLines(ArrayList<CodaDocumentLine> lines){
			this.lines = lines;
	}
	
	public ArrayList<CodaDocumentLine> getLines(){return this.lines;}
	
	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(this);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonInString;
	} //End of toString
	
	
	/**
	 * Sample constructor
	 */
	public CodaDocument(){
		cmpCode="TEST";
		docCode="JOURNAL";
		this.lines.add(new CodaDocumentLine());
	}

	
	public void addLine(CodaDocumentLine line){
		this.lines.add(line);
	}//end of addLine
	
	
	public void setLineAt(int index,CodaDocumentLine line){
		this.lines.set(index, line);
	}
	
}


